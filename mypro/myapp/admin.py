from django.contrib import admin
from myapp.models import ContactForm
# Register your models here.

class AdminContactForm(admin.ModelAdmin):
    list_display = ['name','email','message']
    list_filter  = ['created_on']

admin.site.register(ContactForm,AdminContactForm)