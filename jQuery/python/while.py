#while loop
# a = 0 
# while a > 10 :
#     print(a)
#     a = a+1
# a = 9 
# while a > 0 :
#     print(a)
#     a = a-1

option = '''
    option 1: search color
    option 2: exit
'''
li = ['sharry','garry', 'harry','salash']
print(option)
while True:
    choice_ = input('enter your option')
    if choice_ =='1':
        name_ = input('enter your color')
        if name_ in li :
            print('color {} is at index = {}'.format(name_,li.index(name_)))
        else:
            print('color {} not exists in the list'.format(name_))
    elif choice_ == 2:
        break
    else:
        print('invaild option')

