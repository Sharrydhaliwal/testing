pro = {'name':'Phone','price':'12,000'}
# print(dir(pro))

# print(pro.keys())
# print(pro.values())
# get
# print(pro['name'])
# print(pro.get('name'))

# update
pro['price'] = '15,000'
pro.update({'price':'17,000'})

# Insert
pro['discount'] = '999'
pro['discount'] = '1200'
# print(pro)


# remove
# pro.popitem()
# pro.pop('name')
# del pro['name']
# del pro
# pro.clear()

# print(pro)

# print(pro.items())

for i in pro.items():
    print(i)