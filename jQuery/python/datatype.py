# Integer
a = 103
aa = int()
# Float
b = 122.121
bb = float()
# String
c = 'Hello'
cc = int('1212')
ccc = str()
# List -> array
d = [1,2,4,5,5646]
dd = list()
# Tuple
e = (1,2,45,3453,21)
ee = tuple()
# Dict
f = {'key':'value','key1':'value','key2':'value'}
ff = dict()
fff = {}
# set
g = {1,2,45,645,1,'sd'}
gg = set()
# complex
# h = z+1j
# boolean
i = True

print(g)
print(type(g))