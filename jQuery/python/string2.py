mytxt = 'hello i am sharry dhaliwal'

# #indexing
# print(mytxt[4])

# #reverse indexing 
# print(mytxt[-5])

# # slicing
# print(mytxt[:5])
# print(mytxt[:-5])

# # step size
# print(mytxt[::3])

# #membership
# print('sharry'in mytxt)
# print('dhaliwal' not in mytxt)

# if 'salash'in mytxt:
#     print('string exists')
# else:
#     print('not exists')
mytxt1 = ' hacker'
#concatention
print(mytxt + mytxt1)