
option = '''
        option  1: enter username 
        option  2: show usernames
        option  3: update username
        option  4: delete username
        option  5: exit
'''
print(option)
data = []
while True:
    choice_ = input('enter your option')
    if choice_ == '1':
        entername = input('Enter your name.')
        if entername not in data:
            data.append(entername)
        else:
            print('name is already exists')
    elif choice_ == '2':
        print(data)
    elif choice_ == '3':
        updateName = input('Enter name in the list to update.')
        if updateName in data:
            newName = input('Enter new name to update.')
            findIndex = data.index(updateName)
            data[findIndex] = newName
        else:
            print('Name not in list')
    elif choice_ == '4':
        updateName = input('Enter name in the list to delete.')
        if updateName in data:
            findIndex = data.index(updateName)
            data.pop(findIndex)
        else:
            print('Invalid name')
    elif choice_ == '5':
        break
    else:
        print('invaild option')

