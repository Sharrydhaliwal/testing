# # positional
# def student(name,email,roll_no):
#     print('hi {}, your email {} and your roll no {}'.format(name,email,roll_no))

# student('sharry','sharry@gmail.com',122)
# student('dhaliwal','dhaliwal@gmail.com',124)

#keyword
# def student(name,email,roll_no):
#     print('hi {}, your email {} and your roll no {}'.format(name,email,roll_no))

# student(name='sharry',email='sharry@gmail.com',roll_no=122)
# student('dhaliwal',email='dhaliwal@gmail.com',roll_no=124)
# student(roll_no=121,name='sharry',email='sharry@gmail.com')

#fn defulat
def student(name='',email='',course="python"):
    print('hi {}, your email {} and your course {}'.format(name,email,course))

student('sharry', 'sharry@')
student('sharry', 'sharry@','php')
student()
