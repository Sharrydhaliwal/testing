# variable scoping
name = '''
    dhaliwal
'''
print(name)

def showname():
    x = 'saab'
    global name
    name = '''
        sharry
    '''
    print(name)

showname()
print(name)

def add():
    x = 'waheguru'
    print(name)
add()